$(document).ready(function () {
	'use strict';
	$('a[href^="#"]').bind('click.smoothscroll', function (e) {
		e.preventDefault();
		var target = this.hash,
			$target = $(target);

		$('html, body').stop().animate({
			'scrollTop': $target.offset().top - 30
		}, 900, 'swing', function () {
			window.location.hash = target;
		});
	});
	 $('.parallax').scrolly({bgParallax: true});
	// Animations
	//-----------------------------------------------
	//	if (($("[data-animation-effect]").length > 0) && !Modernizr.touch) {
	//		$("[data-animation-effect]").each(function () {
	//			var $this = $(this),
	//				animationEffect = $this.attr("data-animation-effect");
	//			if (Modernizr.mq('only all and (min-width: 768px)') && Modernizr.csstransitions) {
	//				$this.appear(function () {
	//					var delay = ($this.attr("data-effect-delay") ? $this.attr("data-effect-delay") : 1);
	//					if (delay > 1) $this.css("effect-delay", delay + "ms");
	//					setTimeout(function () {
	//						$this.addClass('animated object-visible ' + animationEffect);
	//					}, delay);
	//				}, {
	//					accX: 0,
	//					accY: -130
	//				});
	//			} else {
	//				$this.addClass('object-visible');
	//			}
	//		});
	//	}

	//Owl Carousel
	//-----------------------------------
	$('.owl-carousel').owlCarousel({
		loop: true,
		margin: 10,
		controlNav: true,
		responsiveClass: true,
		responsive: {
			0: {
				items: 1,
				nav: true
			},
			600: {
				items: 2,
				nav: false
			},
			1000: {
				items: 3,
				nav: true,
				loop: false
			}
		}
	});
});
//Slick Carousel
//-----------------------------------

$('.center').slick({
  centerMode: true,
  centerPadding: '60px',
  slidesToShow: 3,
  responsive: [
    {
      breakpoint: 768,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 3
      }
    },
    {
      breakpoint: 480,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: '40px',
        slidesToShow: 1
      }
    }
  ]
});
$('.customer-section').css({
	background: 'block'
});
$(window).scroll(function () {
	"use strict";
	var scroll = $(window).scrollTop();
	$(".zoom").css({
		backgroundSize: (100 + scroll / 5) + "%",
		top: -(scroll / 10) + "%",

		//Blur suggestion from @janwagner: https://codepen.io/janwagner/ in comments
		//"-webkit-filter": "blur(" + (scroll/200) + "px)",
		//filter: "blur(" + (scroll/200) + "px)"
	});
});
// var current = $(window).scrollTop();
//     var total = $(window).height() - current;
//     var ele = $(".line-motion");
//     var currPosition = ele.position().left + 1000;
//     var trackLength = 350;
//     $(window).scroll(function(event) {
// 		"use strict";
//       current = $(window).scrollTop();
//       console.log({
//         total: total,
//         current: current
//       });
//       console.log(current / total * 100);
//       var newPosition = trackLength * (current / total);
// 	  if (currPosition-newPosition>50){
// 		  ele.css({
// 			left: currPosition - newPosition + 'px'
// 		  });
// 		}
//     });
//     var current = $(window).scrollTop();
//     var total = $(window).height() - current;
//     var ele = $(".section-img-right .line-motion");
//     var currPosition = ele.position().right + 1000;
//     var trackLength = 350;
//     $(window).scroll(function(event) {
// 		"use strict";
//       current = $(window).scrollTop();
//       console.log({
//         total: total,
//         current: current
//       });
//       console.log(current / total * 100);
//       var newPosition = trackLength * (current / total);
// 	  if (currPosition-newPosition>50){
// 		  ele.css({
// 			left: currPosition - newPosition + 'px'
// 		  });
// 		}
//     });
