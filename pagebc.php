<?php get_header();?>
<main class="content">
      <section class="hero-section inner-banner" style="background-image: url(http://highmedia.ca/clients/dispatch/wp-content/uploads/2019/01/top-header-dispatch.jpg);">      

</section>
   <div class="container">
          <?php if(have_posts()):while(have_posts()):the_post();?>
            <?php the_content();?>
          <?php endwhile; endif;?>
    </div>
   </main>   
   
<?php get_footer();?>