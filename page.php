<?php get_header();?>
<main class="content">
      <section class="hero-section inner-banner" style="background-image: url(http://highmedia.ca/clients/dispatch/wp-content/uploads/2019/01/top-header-dispatch-bg.jpg);">      
	<div class="container"> 
		<div class="hero-content">
			<span class="blue-line"></span>
           <h2>THE MOST ADVANCED <span class="blue-text">DISPATCH MANAGEMENT SYSTEM</span></h2>
			<h3>TO UNLOCK THE POTENTIAL OF YOUR DELIVERY BUSINESS.</h3>
                    <p><img src="http://highmedia.ca/clients/dispatch/wp-content/uploads/2018/12/icon.png" alt="Hero Icon"></p>                  
        </div>			

	</div>
</section>
   <div class="container">
          <?php if(have_posts()):while(have_posts()):the_post();?>
            <?php the_content();?>
          <?php endwhile; endif;?>
    </div>
   </main>   
   
<?php get_footer();?>